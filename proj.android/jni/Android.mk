LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path, $(LOCAL_PATH))

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

FILE_LIST := $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/jansson/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/NDKHelper/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/SonarCocosHelperCPP/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Resources/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/AdsLayer/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Shop/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/LuckyWheel/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/LeaderBoard/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Handler/FacebookHandler/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Handler/ParseHandler/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Handler/IAPHandler/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/BExtensions/Model/*.cpp) 

LOCAL_SRC_FILES := hellocpp/main.cpp
LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_LDLIBS := -landroid -llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/jansson
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/NDKHelper
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/SonarCocosHelperCPP
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Resources
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/AdsLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Shop
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/LuckyWheel
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/LeaderBoard
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Handler/FacebookHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Handler/ParseHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Handler/IAPHandler
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/BExtensions/Model
LOCAL_WHOLE_STATIC_LIBRARIES := PluginFacebook sdkbox PluginIAP

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END
LOCAL_STATIC_LIBRARIES := cocos2dx_static
# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./pluginfacebook)
$(call import-module, ./pluginiap)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
