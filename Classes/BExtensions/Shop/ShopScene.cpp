#include "ShopScene.h"
#include "EResources.h"
#include "MenuScene.h"
#include "EConstants.h"
#include "GameScene.h"
#include "Resources.h"
#include "FacebookHandler.h"
#include "ParseHandler.h"
#include "NDKHelper.h"

#define TAG_GET_IMAGE_TO_ADD_TO_SHOP 1996
#define TAG_GET_IMAGES_TO_RUN_ANIMATION 1997

Scene* ShopScene::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = ShopScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool ShopScene::init() {
	//////////////////////////////
	// 1. super init first
	if (!Layer::init()) {
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	selectedItemIndex = UserDefault::getInstance()->getIntegerForKey(KEY_THEME, 0);
	userCoins = 100;

	//Init IAPHandler
	IAPHandler::getInstance()->configIAP();
	IAPHandler::getInstance()->setIAPDelegate(this);

	//Add loading sprite
	loading_sprite = Sprite::create("loading.png");
	loading_sprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	loading_sprite->setPosition(visibleSize / 2);
	this->addChild(loading_sprite, 9999);
	loading_sprite->runAction(RepeatForever::create(RotateBy::create(1, -360)));

	//Background
	Sprite* background = Sprite::create(s_shop_background);
	background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	background->setPosition(visibleSize / 2);
	this->addChild(background);

	//Board
	Sprite* board = Sprite::create(s_shop_board);
	board->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	board->setPosition(visibleSize.width / 2 - 20, visibleSize.height / 2);
	this->addChild(board, 2);

	//Button home
	Button* btnHome = Button::create(s_shop_btn_home);
	btnHome->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
	btnHome->setPosition(Vec2(1260, visibleSize.height - 240));
	btnHome->setTouchEnabled(true);
	btnHome->setPressedActionEnabled(true);
	btnHome->addTouchEventListener(
			CC_CALLBACK_2(ShopScene::homeButtonCallback, this));
	this->addChild(btnHome);

	//Button coins
	Button* btnCoins = Button::create(s_shop_btn_coins);
	btnCoins->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
	btnCoins->setPosition(Vec2(1260, visibleSize.height - 390));
	btnCoins->setTouchEnabled(true);
	btnCoins->setPressedActionEnabled(true);
	btnCoins->addTouchEventListener(
			CC_CALLBACK_2(ShopScene::coinsButtonCallback, this));
	this->addChild(btnCoins);

	//Button diamond
	Button* btnDiamond = Button::create(s_shop_btn_diamond);
	btnDiamond->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
	btnDiamond->setPosition(Vec2(1260, visibleSize.height - 540));
	btnDiamond->setTouchEnabled(true);
	btnDiamond->setPressedActionEnabled(true);
	btnDiamond->addTouchEventListener(
			CC_CALLBACK_2(ShopScene::diamondButtonCallback, this));
	this->addChild(btnDiamond);

	CCLog("bambi moi tao xog het may btton ne");
    ParseHandler::getInstance()->setParseDelegate(this);
    ParseHandler::getInstance()->isInternetConnect();

	//Keyboard handling
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased =
			CC_CALLBACK_2(ShopScene::onKeyReleased,this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener,
			this);
	
    

	return true;
}

void ShopScene::responseForQuerryShopItems() {
	CCLog("bambi response ra shop roi ne");

	auto itemList = ShopItem::vt_ShopItems;
	numberOfImagesNeedToBeDownloaded = itemList.size();


	vt_ItemShop = itemList;
	for (ShopItem* item : itemList) {
		CCLog("bambi for ne:%s", item->getImageURL().c_str());
		//Get image path in local
		std::string writablePath = FileUtils::getInstance()->getWritablePath();
		writablePath.append(item->getName());

		CCLog("bambi check file exitst:%s",writablePath.c_str());
		//If image doesn't exist in local, download it from the internet
		getImageFromInternetTAG = TAG_GET_IMAGE_TO_ADD_TO_SHOP;
		if (!checkFileExistInResource(writablePath.c_str()))
			getImageFromInternet(item->getImageURL(), item->getName());
		else
			numberOfImagesNeedToBeDownloaded--;
	}

	if (numberOfImagesNeedToBeDownloaded == 0)
		addItemsToBoard();

}
void ShopScene::onKeyReleased(EventKeyboard::KeyCode keycode, Event* event) {

	if (EventKeyboard::KeyCode::KEY_ESCAPE == keycode) {
		auto *newScene = GameScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
	}
}
void ShopScene::addItemsToBoard() {
	loading_sprite->setVisible(false);
	string purchasedItemsStr = UserDefault::getInstance()->getStringForKey(KEY_PURCHASED_ITEMS,"");
	//Add items
	int tag = 1;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 4; j++) {
			float positionX = (j + 1) * 252;
			float positionY = visibleSize.height - (i + 1) * 290;

			Sprite* frame_selected = Sprite::create(s_shop_frame_selected);
			frame_selected->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			frame_selected->setPosition(positionX - 10, positionY + 5);
			if (tag != selectedItemIndex)
				frame_selected->setOpacity(0);
			this->addChild(frame_selected, 2);

			//Button shop item
			std::string writablePath =
					FileUtils::getInstance()->getWritablePath();
			writablePath.append(vt_ItemShop[tag - 1]->getName());
			CCLog("bambi button img:%s", writablePath.c_str());
			Button* btnItem = Button::create(writablePath.c_str());
			btnItem->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			btnItem->setPosition(
					Vec2(frame_selected->getContentSize().width / 2,
							frame_selected->getContentSize().height / 2+15));
			btnItem->setTouchEnabled(true);
			btnItem->setPressedActionEnabled(false);
			btnItem->setTag(tag);
			if(checkItemPurchased(tag) == false)
				btnItem->setColor(Color3B(0,0,0));
			btnItem->addTouchEventListener(
					CC_CALLBACK_2(ShopScene::itemButtonCallback, this));
			frame_selected->addChild(btnItem, 2);
			vt_ItemButtons.push_back(btnItem);

			//Price background
			Sprite* price_tag = Sprite::create(s_shop_price_tag);
			price_tag->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			price_tag->setPositionX(frame_selected->getContentSize().width/2);
			price_tag->setPositionY(frame_selected->getContentSize().height/2-btnItem->getContentSize().height/2-5);
			frame_selected->addChild(price_tag,2);

			//Price label
			Label* labelPrice = Label::createWithTTF(CCString::createWithFormat("%d",vt_ItemShop[tag - 1]->getPrice())->getCString(), s_font, 35);
			labelPrice->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			labelPrice->setPosition(price_tag->getContentSize().width/2,price_tag->getContentSize().height/2);
			labelPrice->setColor(Color3B(255,255,255));
			labelPrice->enableOutline(Color4B(255,255,255,255), 1);
			price_tag->addChild(labelPrice);


			tag++;
			if (tag > vt_ItemShop.size())
				return;
		}
}
void ShopScene::itemButtonCallback(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {

		//Show loading sprite
		loading_sprite->setVisible(true);

		CCLog("bambi itemcallback ne");
		selectedItemButton = static_cast<Button*>(pSender);
		int tag = selectedItemButton->getTag();



		if(vt_ItemShop[tag - 1]->getPrice() > userCoins)
		{
			//Show toast
			CCDictionary* prms = CCDictionary::create();
			prms->setObject(CCString::create("Not enough coins!"), "messageToShow");
			SendMessageWithParams(string("showToast"), prms);

			loading_sprite->setVisible(false);
			return;
		}


		vector<ItemAnimationImage*> vt_itemAnimationImage = vt_ItemShop[tag - 1]->getVt_ItemAnimationImages();
		numberOfImagesNeedToBeDownloaded = vt_itemAnimationImage.size();
		CCLog("bambi itemcallback numberOfImagesNeedToBeDownloaded:%d",numberOfImagesNeedToBeDownloaded);
		for (int i = 0; i < vt_itemAnimationImage.size(); i++) {

			CCLog("bambi itemcallback, vong for thu:%d, size for:%d",i,numberOfImagesNeedToBeDownloaded);

			ItemAnimationImage* animationImageItem = vt_itemAnimationImage[i];
			string imageIndexStr = CCString::createWithFormat("%d",animationImageItem->getImageIndex())->getCString();

			//Get image path in local
			std::string writablePath =
					FileUtils::getInstance()->getWritablePath();
			writablePath.append(imageIndexStr+vt_ItemShop[tag - 1]->getName());


			CCLog("bambi itemcallback, check file nay:%s",writablePath.c_str());

			//If image doesn't exist in local, download it from the internet
			getImageFromInternetTAG = TAG_GET_IMAGES_TO_RUN_ANIMATION;
			if (!checkFileExistInResource(writablePath.c_str()))
			{
				getImageFromInternet(animationImageItem->getImageURL(),imageIndexStr+vt_ItemShop[tag - 1]->getName());
				CCLog("bambi itemcallback file nay chua co");
			}
			else
			{
				numberOfImagesNeedToBeDownloaded--;
				CCLog("bambi itemcallback file nay co roi");
			}
		}

		CCLog("bambi itemcallback ne, size:%d",numberOfImagesNeedToBeDownloaded);
		if (numberOfImagesNeedToBeDownloaded <= 0)
			selectItem();

	}
}
bool ShopScene::checkItemPurchased(int itemIndex)
{
	string purchasedItemsStr = UserDefault::getInstance()->getStringForKey(KEY_PURCHASED_ITEMS,"");
	if(purchasedItemsStr.find(CCString::createWithFormat("%d,",itemIndex)->getCString()) != std::string::npos)
	{
		CCLog("bambi check purchased return true");

		return true;
	}
	CCLog("bambi check purchased return false");
	return false;
}
void ShopScene::appendPurchasedItems(int itemIndex)
{
	string purchasedItemsStr = UserDefault::getInstance()->getStringForKey(KEY_PURCHASED_ITEMS,"");
	purchasedItemsStr += CCString::createWithFormat("%d,",itemIndex)->getCString();
	UserDefault::getInstance()->setStringForKey(KEY_PURCHASED_ITEMS,purchasedItemsStr);
}
void ShopScene::selectItem()
{
	for (Button* button : vt_ItemButtons)
			button->getParent()->setOpacity(0);

	selectedItemButton->getParent()->setOpacity(255);
	selectedItemButton->setColor(Color3B(255,255,255));
	selectedItemIndex = selectedItemButton->getTag()-1;
	UserDefault::getInstance()->setIntegerForKey(KEY_THEME, selectedItemIndex+1);


	CCLog("bambi vao select item, tag:%d",selectedItemIndex);

	string playerAnimationString = "";
	string animationName = vt_ItemShop[selectedItemIndex]->getName();

	CCLog("bambi vao select item,animationName:%s",animationName.c_str());

	vector<ItemAnimationImage*> itemAnimationImage = vt_ItemShop[selectedItemIndex]->getVt_ItemAnimationImages();
	CCLog("bambi test size cua vt anim:%d",itemAnimationImage.size());
	for(int i = 0 ; i < itemAnimationImage.size() ; i++)
		if(i != itemAnimationImage.size() - 1)
			playerAnimationString += CCString::createWithFormat("%d",itemAnimationImage[i]->getImageIndex())->getCString() + animationName+",";
		else
			playerAnimationString += CCString::createWithFormat("%d",itemAnimationImage[i]->getImageIndex())->getCString() + animationName;

	CCLog("bambi vt anim:%s",playerAnimationString.c_str());
	UserDefault::getInstance()->setStringForKey(KEY_PLAYER_ANIMATION, playerAnimationString);

	//Tru` tien` neu nhu chua mua item nay
	if(checkItemPurchased(selectedItemIndex+1) == false)
	{
		userCoins -= vt_ItemShop[selectedItemIndex]->getPrice();
		auto stringScore = CCString::createWithFormat("Coins: %d",userCoins)->getCString();
		lblScore->setString(stringScore);
		ParseHandler::getInstance()->submitScore(-vt_ItemShop[selectedItemIndex]->getPrice());
		appendPurchasedItems(selectedItemIndex+1);
	}

	//Hide loading sprite
	loading_sprite->setVisible(false);
}
void ShopScene::homeButtonCallback(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
		auto *newScene = MenuScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
	}
}
void ShopScene::coinsButtonCallback(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED && FacebookHandler::getInstance()->isFacebookLoggedIn()) {
		IAPHandler::getInstance()->purchaseItem("100_coins");
	}
}
void ShopScene::diamondButtonCallback(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
		IAPHandler::getInstance()->purchaseItem("remove_ads");
	}
}

void ShopScene::responseAfterGetCoinsFromParse(int score){
    
	userCoins = score;
    CCLOG("ShopScene responseAfterGetCoinsFromParse");
    
    CCLOG("ShopScene Get Score From PareHandler: %d", score);
    
    auto stringScore = CCString::createWithFormat("Coins: %d",score)->getCString();
    
    lblScore = Label::createWithTTF(stringScore, s_font, 50);
    lblScore->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    lblScore->setPosition(Vec2(visibleSize.width,visibleSize.height));
    this->addChild(lblScore);
    
    //Init IAPHandler
    IAPHandler::getInstance()->configIAP();
    IAPHandler::getInstance()->setIAPDelegate(this);
    
    //After getAvailableItemsOnServerCallback, responseForQuerryShopItems function will be called
    ParseHandler::getInstance()->getAvailableItemsOnServer();
    
}

void ShopScene::responseWhenGetItemSuccessfully(string itemID) {
	if (itemID == "remove_ads")
		UserDefault::getInstance()->setBoolForKey("remove_ads", true);
	else if (itemID == "100_coins") {
		int coins = UserDefault::getInstance()->getIntegerForKey("coins", 0);
		UserDefault::getInstance()->setIntegerForKey("coins", 100 + coins);
	}
}
void ShopScene::responseWhenLoginOrLogoutFacebook() {

}
void ShopScene::responseForQuerryTopWorld(vector<BUserInfor*> worldList) {

}
void ShopScene::responseForQuerryTopFriend(vector<BUserInfor*> worldList) {

}
void ShopScene::responseAfterCheckFacebookIDExistOnParse() {

}
void ShopScene::responseAfterCheckInstalledGamesOnParse() {

}

void ShopScene::getImageFromInternet(string url, string name) {

	if(getImageFromInternetTAG == TAG_GET_IMAGES_TO_RUN_ANIMATION)
		CCLog("bambi vao getImg: name:%s, url:%s",name.c_str(),url.c_str());


	HttpRequest* request = new HttpRequest();
	request->setUrl(url.c_str());
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(
			CC_CALLBACK_2(ShopScene::callBackGetImageFromInternet, this));
	request->setTag(name.c_str()); //Set image name via setting tag
	HttpClient::getInstance()->send(request);
	request->release();
}

void ShopScene::callBackGetImageFromInternet(HttpClient* client,
		HttpResponse* response) {

	HttpResponse* pResponse = response;
	if (!pResponse) {
		CCLOG("No Respose");
		return;
	}
	if (!pResponse->isSucceed()) {
		CCLOG("resposne failed");
		CCLOG("error buffer: %s", pResponse->getErrorBuffer());
		return;
	}
	// Dump the data
	std::vector<char> *buffer = pResponse->getResponseData();

	//Init image from buffer
	Image* img = new Image();
	img->initWithImageData((unsigned char*) &(buffer->front()), buffer->size());
	Texture2D* texture = new Texture2D();
	texture->initWithImage(img);

	//Save image to local
	Sprite* mySprite = Sprite::createWithTexture(texture);
	mySprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	RenderTexture* rtX = RenderTexture::create(mySprite->getContentSize().width,
			mySprite->getContentSize().height);
	rtX->begin();
	mySprite->visit();
	rtX->end();
	rtX->saveToFile(pResponse->getHttpRequest()->getTag(), Image::Format::PNG,true,[&](RenderTexture* rt, const std::string& path)
	{
		//After save successfully
		numberOfImagesNeedToBeDownloaded--;

		CCLog("bambi down tam hinh thu:%d",numberOfImagesNeedToBeDownloaded);
		//Check if this response is the last one
		if (numberOfImagesNeedToBeDownloaded <= 0) {
			CCLog("bambi down tam hinh cuoi cung roi");

			if (getImageFromInternetTAG == TAG_GET_IMAGES_TO_RUN_ANIMATION)
				selectItem();
			else if (getImageFromInternetTAG == TAG_GET_IMAGE_TO_ADD_TO_SHOP)
				addItemsToBoard();
		}
	});


}

bool ShopScene::checkFileExistInResource(const char* pszFileName) {
	return FileUtils::getInstance()->isFileExist(pszFileName);
}

void ShopScene::responseAfterFetchInternetStatus(bool isConnected){
    CCLog("bambi dang o shopscene response after ne");
    if (isConnected == false) {
        
        //Error label
        Label* labelError = Label::createWithTTF("Connect Internet then try again, Please!!!", s_font, 80);
        labelError->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        labelError->setPosition(visibleSize/2);
        labelError->setColor(Color3B(255,255,255));
        labelError->enableOutline(Color4B(255,255,255,255), 1);
        this->addChild(labelError,9999);
        
        loading_sprite->setVisible(false);
        
        return;
    }
    
    if(FacebookHandler::getInstance()->isFacebookLoggedIn() == false)
    {
        //Error label
        Label* labelError = Label::createWithTTF("Login Facebook then try again.", s_font, 80);
        labelError->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        labelError->setPosition(visibleSize/2);
        labelError->setColor(Color3B(255,255,255));
        labelError->enableOutline(Color4B(255,255,255,255), 1);
        this->addChild(labelError,9999);
        
        loading_sprite->setVisible(false);
        
        return;
    }
    
    CCLOG("Chay Xuong Hok???");
    //ParseHandler::getInstance()->setParseDelegate(this);
    ParseHandler::getInstance()->fetchScoreFromParse();

    
}


