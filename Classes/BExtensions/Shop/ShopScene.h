#ifndef __SHOP_SCENE_H__
#define __SHOP_SCENE_H__

#include "cocos2d.h"
#include "cocos/ui/CocosGUI.h"
#include "IAPHandler.h"
#include "ParseHandler.h"

USING_NS_CC;
using namespace std;
using namespace cocos2d::ui;

class ShopScene : public cocos2d::Layer, public ParseDelegate, public IAPDelegate
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    // implement the "static create()" method manually
    CREATE_FUNC(ShopScene);
    

    void addItemsToBoard();
    void onKeyReleased(EventKeyboard::KeyCode keycode, Event* event);
	void homeButtonCallback(Ref* pSender,ui::Widget::TouchEventType eEventType);
	void coinsButtonCallback(Ref* pSender,ui::Widget::TouchEventType eEventType);
	void diamondButtonCallback(Ref* pSender,ui::Widget::TouchEventType eEventType);
	void itemButtonCallback(Ref* pSender,ui::Widget::TouchEventType eEventType);
	void selectItem();
	bool checkItemPurchased(int itemIndex);
	void appendPurchasedItems(int itemIndex);
	vector<Button*> vt_ItemButtons;
	vector<ShopItem*> vt_ItemShop;
	int selectedItemIndex;
	int userCoins;
	Button* selectedItemButton;
    Sprite* loading_sprite;
    Label* lblScore;

    Size visibleSize;
    Vec2 origin;


       	//Responsed function
    virtual void responseWhenGetItemSuccessfully(string itemID); //From IAPHandler
    virtual void responseWhenLoginOrLogoutFacebook(); //From FacebookHandler
    virtual void responseForQuerryTopWorld(vector<BUserInfor*> worldList); //From ParseHandler
    virtual void responseForQuerryTopFriend(vector<BUserInfor*> worldList); //From ParseHandler
    virtual void responseAfterCheckFacebookIDExistOnParse(); //From ParseHandler
    virtual void responseAfterCheckInstalledGamesOnParse(); //From ParseHandler
    virtual void responseForQuerryShopItems(); //From ParseHandler
    virtual void responseAfterGetCoinsFromParse(int score); //From ParseHandle
    virtual void responseAfterFetchInternetStatus(bool isConnected);    //From Parsehandle
    

       		   //Download image
       		   	void getImageFromInternet(string url, string name);
       		    void callBackGetImageFromInternet(HttpClient* client, HttpResponse* response);
       		    bool checkFileExistInResource (const char* pszFileName );
       		    int numberOfImagesNeedToBeDownloaded;
       		    int getImageFromInternetTAG;
};

#endif
