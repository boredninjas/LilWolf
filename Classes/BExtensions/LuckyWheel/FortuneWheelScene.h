#ifndef __FORTUNEWHEEL_SCENE_H__
#define __FORTUNEWHEEL_SCENE_H__

#include "cocos2d.h"
#include <ui/CocosGUI.h>
#include <cocos/network/HttpRequest.h>
#include <cocos/network/HttpClient.h>
#include <json/rapidjson.h>
#include <json/document.h>

#include <curl/include/ios/curl/curl.h>
#include <curl/include/android/curl/curl.h>

#include "ParseHandler.h"
using namespace cocos2d::network;
using namespace cocos2d;
using namespace rapidjson;
using namespace cocos2d::ui;
using namespace std;

class FortuneWheel : public cocos2d::Layer, public ParseDelegate
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(FortuneWheel);
    
    Size visibleSize;
    
    Vec2 origin;
    
private:
    int myFreeSpin;
    int maxSpins;
    long myCoin;
    bool isClickable;
    bool isSound;
    
    //add Loading Sprite
    Sprite *loadingSprite;
    
    //add Wheel
    Sprite *wheel;
    
    //add ProgressBar
    Sprite *progressBarBackground;
    
    //add loadingBar
    LoadingBar *loadingBar;
    
    //add scoreLabel
    Label *scoreLabel;
    
    //add SpinLabel
    Label *spinLabel;
    
    //Add Button Spin
    Button *btnSpin;
    
    
    


    void addGameComponents();
    void EventTouchCallBack();
    void spinTheWheel();
    int getRandomSectionWithCriteria();
    void updateSpinUI();
    void checkInternetThenGoToShop();
    bool onTouchBegan(Touch *touch, Event *event);
    void checkTime();
    void homeButtonCallback(Ref* pSender,
    		ui::Widget::TouchEventType eEventType);
    void onKeyReleased(EventKeyboard::KeyCode keycode, Event* event);
    
    
    /*************************
     **** Parse Delegate *****
     *************************/
    
    //All Scene
    virtual void responseAfterGetCoinsFromParse(int score);
    virtual void responseAfterFetchInternetStatus(bool isConnected);
    
};

#endif // __FORTUNEWHEEL_SCENE_H__
