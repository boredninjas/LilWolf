#include "FortuneWheelScene.h"
#include "EResources.h"
#include <SimpleAudioEngine.h>
#include "EConstants.h"
#include "CppUtils.h"
#include "Resources.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "FacebookHandler.h"
#include "ParseHandler.h"

USING_NS_CC;

Scene* FortuneWheel::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = FortuneWheel::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FortuneWheel::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    
    //demo loadingBar
    UserDefault::getInstance()->setIntegerForKey(KEY_DATE, 20151119);
    //maxSpins = 50;
//    myFreeSpin = 20;
    
    isClickable = true;
    isSound = true;
    
    //Add Loading Sprite
    loadingSprite = Sprite::create(s_wheelscene_loading);
    loadingSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    loadingSprite->setPosition(visibleSize / 2);
    this->addChild(loadingSprite, 1000);
    loadingSprite->runAction(RepeatForever::create(RotateBy::create(1.5, 360)));
    loadingSprite->setVisible(false);
    
    //Play Game
    addGameComponents();
    
    //Check Date
    checkTime();
    
	//Button home
	Button* btnHome = Button::create(s_shop_btn_home);
	btnHome->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
	btnHome->setPosition(Vec2(visibleSize));
	btnHome->setTouchEnabled(true);
	btnHome->setPressedActionEnabled(true);
	btnHome->addTouchEventListener(
			CC_CALLBACK_2(FortuneWheel::homeButtonCallback, this));
	this->addChild(btnHome);

	//Keyboard handling
		auto keyboardListener = EventListenerKeyboard::create();
		keyboardListener->onKeyReleased =
				CC_CALLBACK_2(FortuneWheel::onKeyReleased,this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener,
				this);

    ParseHandler::getInstance()->setParseDelegate(this);
    ParseHandler::getInstance()->isInternetConnect();
    
    return true;
}
void FortuneWheel::onKeyReleased(EventKeyboard::KeyCode keycode, Event* event) {

	if (EventKeyboard::KeyCode::KEY_ESCAPE == keycode) {
		auto *newScene = GameScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
	}
}
void FortuneWheel::homeButtonCallback(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
        
		auto *newScene = MenuScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
        
	}
}
void FortuneWheel::addGameComponents(){
    
    //Add background
    Sprite *background = Sprite::create(s_wheelscene_bg);
    background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    background->setPosition(visibleSize.width / 2, visibleSize.height / 2);
    this->addChild(background, 0);
    
    //Add The Wheel
    wheel = Sprite::create(s_wheelscene_wheel);
    wheel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    wheel->setPosition(visibleSize.width /2, visibleSize.height * 0.5);
    this->addChild(wheel, 2);
    
    //Add The Wheel Center
    Sprite *wheelCenter = Sprite::create(s_wheelscene_wheelcenter);
    wheelCenter->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    wheelCenter->setPosition(wheel->getPosition().x, wheel->getPosition().y + 30);
    this->addChild(wheelCenter, 3);
    //updateSpinUI();
    
    //Add Label Score
    scoreLabel = Label::createWithTTF(CCString::createWithFormat("%ld",myCoin)->getCString(), s_font, 25);
    scoreLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    scoreLabel->setPosition(Vec2(visibleSize.width + 0.5 - 65,visibleSize.height * 0.7 + 23));
//    this->addChild(scoreLabel,5);

    
    //Add Label My Free Spin
    spinLabel = Label::createWithTTF(CCString::createWithFormat("FreeSpin: %d", myFreeSpin)->getCString(), s_font, 25);
    spinLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    spinLabel->setPosition(Vec2(visibleSize.width - spinLabel->getContentSize().width - 10, visibleSize.height * 0.7 + 23));
//    this->addChild(spinLabel,5);
    
    
}

void FortuneWheel::EventTouchCallBack(){
    CCLOG("Chay Di Pa");
            spinTheWheel();
            myFreeSpin--;
            //****updateSpinUI();
            
//            //Show ads every 5 spins
//            int spinTimes = UserDefault::getInstance()->getIntegerForKey("GAME_OVER_TIMES",1);
//            if(spinTimes >= 3)
//            {
////                SonarCocosHelper::AdMob::showFullscreenAd();
//                UserDefault::getInstance()->setIntegerForKey("GAME_OVER_TIMES",1);
//            }
//            else{
//                UserDefault::getInstance()->setIntegerForKey("GAME_OVER_TIMES",spinTimes+1);
//            }
    
}
void FortuneWheel::spinTheWheel(){
    isClickable = false;
    
    //Rotate the spin
    int randSectionIndex = getRandomSectionWithCriteria();
    CCLOG("Check Wheel_RandSectionIndex: %d", randSectionIndex);
    float angleToRotate = 360 - (360/8*randSectionIndex + (int)wheel->getRotation()%360);
    if(angleToRotate<0)
        angleToRotate += 720;
    float realRotateDuration = angleToRotate/720;
    CCLOG("Check Wheel_RealRotateDuration: %f", realRotateDuration);
    RotateBy* realRotate = RotateBy::create(realRotateDuration,angleToRotate);
    Vector<FiniteTimeAction*> vt_actions;
    vt_actions.pushBack(RotateBy::create(1,720));
    float velocity = 720/1; //720 degrees/s
    for(int i=1 ; i<=40 ; i++)
    {
        if(i!=40)
        {
            if(i==39)
                velocity-=9;
            else
                velocity-=18;
        }
        RotateBy* ornamentalRotate = RotateBy::create(18/velocity,18);
        vt_actions.pushBack(ornamentalRotate);
    }
    Sequence* seq = Sequence::create(vt_actions);
    wheel->runAction(Sequence::create(realRotate,seq,nullptr));
    
    
    auto funcAfterSpin = CallFunc::create([=]() {
        isClickable = true;
        
        if(vt_sections_score[randSectionIndex] != 0 && vt_sections_score[randSectionIndex] != 7777) //If get coins (not Win and not Lose)
        {
            myCoin+=vt_sections_score[randSectionIndex];
            
            CCLOG("Vòng Xoay May Mắn: %ld Tiền", myCoin);
            
            //Submit score to server
            ParseHandler::getInstance()->submitScore(vt_sections_score[randSectionIndex]);

            //Update label score
            auto stringScore = CCString::createWithFormat("Coins: %d",myCoin)->getCString();
            scoreLabel->setString(stringScore);

            //Show particle
            ParticleSystemQuad* particle = ParticleSystemQuad::create(s_wheelscene_particle);
            particle->setPosition(visibleSize.width/2,visibleSize.height*0.6);
            particle->setDuration(0.3);
            particle->setAutoRemoveOnFinish(true);
            this->addChild(particle,1000);
        }
        else
        {
            //Effect sprite
            Sprite* effectSprite = Sprite::create(s_wheelscene_skull);
            effectSprite->setPosition(visibleSize.width/2,visibleSize.height*0.6);
            effectSprite->setScale(2);
            this->addChild(effectSprite,1000);
            //Action
            FadeOut* fadeOut = FadeOut::create(1.5);
            ScaleBy* scale = ScaleBy::create(1.5,3);
            MoveBy* moveUp = MoveBy::create(1.5,Vec2(0,100));
            Spawn* spawn = Spawn::createWithTwoActions(fadeOut,scale);
            auto func = CallFunc::create([=]() {
                effectSprite->removeFromParent();
            });
            effectSprite->runAction(Sequence::create(Spawn::createWithTwoActions(spawn,moveUp),func,nullptr));
            
            
            //Result after spin
            if(vt_sections_score[randSectionIndex] == 7777) //Win
            {
                effectSprite->setTexture(Director::getInstance()->getTextureCache()->addImage(s_wheelscene_bottle));
                CCLOG("Vòng Xoay May Mắn: Được Chiếc Hộp");
                
                //effectSprite->setTexture(CCTextureCache::sharedTextureCache()->addImage(s_fortunewheelscene_bottle));
                //Update number of free spin
                //myFreeSpin+=5;
                //*****updateSpinUI();
            }
            else //Lose
            {
                CCLOG("Vòng Xoay May Mắn: Thua");
                effectSprite->setTexture(Director::getInstance()->getTextureCache()->addImage(s_wheelscene_skull));
                //                effectSprite->setTexture(CCTextureCache::sharedTextureCache()->addImage(s_fortunewheelscene_skull));
            }
        }
    });
    auto seqFunc = Sequence::create(DelayTime::create(seq->getDuration()+realRotate->getDuration()),funcAfterSpin, nullptr);
    this->runAction(seqFunc);
}

int FortuneWheel::getRandomSectionWithCriteria() {
    int minRand = 0;
    int rand = CCRANDOM_0_1()*101;
    for (int i = 0; i < vt_sections_percentage.size(); i++) {
        if (minRand <= rand && rand <= minRand + vt_sections_percentage[i]){
            
            CCLOG("%d", i);
            return i;
        }
        else{
           minRand += vt_sections_percentage[i];
            
             CCLOG("%d", minRand);
        }
        

    }
    return 0;
}

void FortuneWheel::checkInternetThenGoToShop(){
    loadingSprite->setVisible(true);
    
    HttpRequest *r = new HttpRequest();
    r->setUrl("https://google.com");
    r->setRequestType(HttpRequest::Type::GET);
    r->setResponseCallback(
                           [=](network::HttpClient *client, network::HttpResponse *response){
                               if (response->isSucceed()) {
                                   CCLOG("Kết Nối Thành Công");
                                   //go To ... Scene
                                   
                               }else{
                                   CCLOG("Mất Kết Nối");
                               }
                               loadingSprite->setVisible(false);
                           }
                           );
    
    HttpClient::getInstance()->send(r);
    r->release();
    
}

bool FortuneWheel::onTouchBegan(Touch *touch, Event *event){
    if (myFreeSpin <= 0) {
        CCLOG("Hay Quay Lai Vao Ngay Mai Nha");
    }else{
        EventTouchCallBack();
    }
    
    return true;
}

void FortuneWheel::checkTime(){
    timeval curTime;
    gettimeofday(&curTime, NULL);
    int milli = curTime.tv_usec / 1000;
    //    float day = curTime.tv_sec;
    
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    //strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
    strftime(buffer, 80, "%Y%m%d", timeinfo);
    
    int nowDate = std::atoi(buffer);
    
    int CurrentDat = UserDefault::getInstance()->getIntegerForKey(KEY_DATE, 1);
    
    if (CurrentDat == 1) {
        UserDefault::getInstance()->setIntegerForKey(KEY_DATE, nowDate);
    }else if (CurrentDat < nowDate){
        myFreeSpin = 1;
        UserDefault::getInstance()->setIntegerForKey(KEY_DATE, nowDate);
    }else if (CurrentDat >= nowDate){
        myFreeSpin = 0;
    }
}

/*************************
 **** Parse Delegate *****
 *************************/

//All Scene
void FortuneWheel::responseAfterGetCoinsFromParse(int score){
	myCoin = score;

    CCLOG("FortuneWheel responseAfterGetCoinsFromParse");
    
    CCLOG("FortuneWheel Get Score From PareHandler: %d", score);
    
    auto stringScore = CCString::createWithFormat("Coins: %d",score)->getCString();

    scoreLabel= Label::createWithTTF(stringScore, s_font, 50);
    scoreLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    scoreLabel->setPosition(Vec2(0,visibleSize.height));
    this->addChild(scoreLabel);
    
    
};

//Check Connection
void FortuneWheel::responseAfterFetchInternetStatus(bool isConnected){
    CCLog("bambi dang o wheel scene roi ne");
    if (isConnected == false) {
        
        //Error label
        Label* labelError = Label::createWithTTF("Connect Internet then try again, Please!!!", s_font, 80);
        labelError->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        labelError->setPosition(visibleSize/2);
        labelError->setColor(Color3B(255,255,255));
        labelError->enableOutline(Color4B(255,255,255,255), 1);
        this->addChild(labelError,9999);
        
        //loading_sprite->setVisible(false);
        
        return;
    }
    
    if(FacebookHandler::getInstance()->isFacebookLoggedIn() == false)
    {
        //Error label
        Label* labelError = Label::createWithTTF("Login Facebook then try again.", s_font, 80);
        labelError->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        labelError->setPosition(visibleSize/2);
        labelError->setColor(Color3B(255,255,255));
        labelError->enableOutline(Color4B(255,255,255,255), 1);
        this->addChild(labelError,9999);
        
        return;
    }
    
    //Handling touch event
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(FortuneWheel::onTouchBegan , this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    ParseHandler::getInstance()->fetchScoreFromParse();
    
}





