
#ifndef _EResources_h
#define _EResources_h

//AdsLayer
static const char s_adslayer_particle[] = "ads/particle_ads.plist";

//Shop
static const char s_shop_board[] = "shopscene/board.png";
static const char s_shop_background[] = "shopscene/background.png";
static const char s_shop_frame_selected[] = "shopscene/frame_selected.png";
static const char s_shop_price_tag[] = "shopscene/price_tag.png";
static const char s_shop_btn_coins[] = "shopscene/btn_coins.png";
static const char s_shop_btn_home[] = "shopscene/btn_home.png";
static const char s_shop_btn_diamond[] = "shopscene/btn_diamond.png";
static const char* s_shop_items[] = {"shopscene/1.png","shopscene/2.png","shopscene/3.png","shopscene/4.png","shopscene/5.png","shopscene/6.png","shopscene/7.png","shopscene/8.png"};

//Wheel
static const char s_wheelscene_bg[] = "wheelscene/bg_wheel1.png";
static const char s_wheelscene_wheel[] = "wheelscene/wheel1.png";
static const char s_wheelscene_wheelcenter[] = "wheelscene/wheelcenter1.png";
static const char s_wheelscene_loading[] = "wheelscene/loading1.png";
static const char s_wheelscene_particle[] = "wheelscene/particle_texture1.plist";
static const char s_wheelscene_skull[] = "wheelscene/skull1.png";
static const char s_wheelscene_bottle[] = "wheelscene/bottle1.png";

//Leaderboard
static const char s_ranking_title[] = "rankingscene/txt_learderboard1.png";
static const char s_ranking_number[] = "rankingscene/my_number1.png";
static const char s_ranking_board[] = "rankingscene/board1.png";
static const char s_ranking_btnHome[] = "rankingscene/btn_home1.png";
static const char s_ranking_btnPlay[] = "rankingscene/btn_play1.png";
static const char s_ranking_background[] = "rankingscene/bg_ranking1.png";
static const char s_ranking_loadingSprite[] = "rankingscene/loading1.png";
static const char s_ranking_loadingSpriteChild[] = "rankingscene/loading_apple1.png";
static const char s_ranking_btnWorldMode[] = "rankingscene/btn_worldMode1.png";
static const char s_ranking_btnWorldMode_Clicked[] = "rankingscene/btn_worldMode_CLicked1.png";
static const char s_ranking_btnFriendMode[] = "rankingscene/btn_friendMode1.png";
static const char s_ranking_btnFriendMode_CLicked[] = "rankingscene/btn_friendMode_Clicked1.png";
static const char s_ranking_btnConnectFacebook_LogOut[] = "rankingscene/btn_connectFB_LogOut1.png";
static const char s_ranking_btnConnectFacebook_LogIn[] = "rankingscene/btn_connectFB_LogIn1.png";

#endif
