
#ifndef _EContanst_h
#define _EContanst_h

#include "cocos2d.h"
using namespace std;

//---------- Parse.com
#define APP_ID "X-Parse-Application-Id: NowxMMqQy41HIZfgtSPK6fqSvxJGr2PS3XxwWHjf"
#define REST_API "X-Parse-REST-API-Key: D1IPNshGLcQTRmnwktdjFnyyJsnTFppBvskPGpLr"
#define CLASS_URL_USER "https://api.parse.com/1/classes/BUser"
#define CLASS_URL_SHOP "https://api.parse.com/1/classes/Shop"
#define CLASS_URL_ANIMATION_IMAGES "https://api.parse.com/1/classes/ItemAnimation"
#define CLASS_URL_ADS "https://api.parse.com/1/classes/Ads"

//---------- Ads layer
#define KEY_WORLD_ADS_IMAGE   "Image"
#define KEY_WORLD_ADS_GAME_NAME   "GameName"
#define KEY_WORLD_ADS_LINK_PLAY_STORE   "PlayStoreLink"
#define KEY_WORLD_ADS_LINK_APP_STORE   "AppStoreLink"
#define KEY_WORLD_ADS_IS_AVAILABLE   "IsAvailable"


//---------- Leaderboard
#define RANKINGWORLDMODE "isrankingworldmode"
#define KEY_HIGH_SCORE "KEYHIGHSCORE"
//Column names on parse
#define KEY_WORLD_NAME       "FB_Name"
#define KEY_WORLD_LOCATION   "FB_Location"
#define KEY_WORLD_ID         "FB_ID"
#define KEY_WORLD_SCORE      "Coin"
#define KEY_WORLD_OJECTID    "objectId"
#define KEY_WORLD_INSTALLED_GAMES    "InstalledGame"
#define KEY_IS_FIRST_TIME_LOGIN_INTO_THIS_GAME "isfirsttimeloginintothisgame"
#define THIS_GAME_NAME "LilWolf"

//---------- Shop
#define KEY_DATE "KEYDATE"
#define KEY_PURCHASED_ITEMS "purchaseditem"
//Column names on parse
#define KEY_WORLD_ITEM_NAME       "ItemName"
#define KEY_WORLD_ITEM_IMAGE_URL   "ItemImage"
#define KEY_WORLD_ITEM_PRICE         "ItemPrice"
#define KEY_WORLD_ITEM_IS_AVAILABLE "IsAvailable"
#define KEY_WORLD_ITEM_ANIMATION_INDEX "AnimationIndex"

//---------- Wheel
#define percentage_section1 25 //0
#define percentage_section2 5 //HopQua
#define percentage_section3 5 //1500
#define percentage_section4 15 //500
#define percentage_section5 25 //0
#define percentage_section6 5 //HopQua
#define percentage_section7 5 //1500
#define percentage_section8 15 //500
static vector<int> vt_sections_percentage = {percentage_section1,percentage_section2,percentage_section3,percentage_section4,percentage_section5,percentage_section6,percentage_section7,percentage_section8};
static vector<int> vt_sections_score = {0, 7777, 1500, 500, 0, 7777, 1500, 500};

//----------Coin
#define KEY_SCORE = "KEYSCORE"


#endif
