
#include "AdsLayer.h"
#include "EResources.h"
#include "EConstants.h"

#define TAG_LOAD_ADS 1996
#define TAG_LOAD_AND_SHOW_ADS 1997

AdsLayer::~AdsLayer()
{
	CCLog("bambi hàm dủy adslayer");
	for(HttpRequest* request : vt_httpRequest)
	{
		request->setResponseCallback(nullptr);
		request->release();
	}
	vt_httpRequest.clear();
}
bool AdsLayer::init()
{
	if (!Layer::init()) {
		return false;
	}
	this->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

	return true;
}
void AdsLayer::addDialog()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();

	//Create menu to touch
	Sprite* sprite_temp = Sprite::createWithTexture(textureAds);
	sprite_temp->setOpacity(0);
	sprite_temp->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	MenuItemSprite* startGameItem = MenuItemSprite::create(sprite_temp, sprite_temp, CC_CALLBACK_0(AdsLayer::onAdsClick,this));
	Menu *menu = Menu::create(startGameItem, NULL);
	menu->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	menu->setPosition(visibleSize.width-sprite_temp->getContentSize().width/2,sprite_temp->getContentSize().height/2);
	this->addChild(menu);


	//Ads sprites
	Sprite* adsSprite = Sprite::createWithTexture(textureAds);
	adsSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	adsSprite->setOpacity(0);
	adsSprite->setPosition(visibleSize.width,0);
	this->addChild(adsSprite,2);


	//Appearing Effect
	auto fadeIn = FadeIn::create(2.0f);
	adsSprite->runAction(fadeIn);

	//Effect
	auto scale = ScaleBy::create(0.8f, 1.05);
	auto seq = Sequence::create(scale, scale->reverse(), nullptr);
	adsSprite->runAction(RepeatForever::create(seq));

	//Partcile
	ParticleSystemQuad* winParticle = ParticleSystemQuad::create(s_adslayer_particle);
	winParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
	winParticle->setDuration(1);
	winParticle->setScale(0.5);
	winParticle->setPosition(Vec2(adsSprite->getContentSize().width/2,adsSprite->getContentSize().height/2));
	adsSprite->addChild(winParticle,999);
}

void AdsLayer::onAdsClick()
{
	#if(CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
			Application::getInstance()->openURL(adsObjects.playStoreLink);
	#endif
	#if(CC_PLATFORM_IOS == CC_TARGET_PLATFORM)
			Application::getInstance()->openURL(adsObjects.appStoreLink);
	#endif
}
void AdsLayer::getAdsFromParse()
{
	//query string
	char query[200];
	sprintf(query, "where={\"%s\":true,\"%s\":{\"$nin\":[\"%s\"]}}", KEY_WORLD_ADS_IS_AVAILABLE,KEY_WORLD_ADS_GAME_NAME,THIS_GAME_NAME);
	char url[55500];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	sprintf(url, "%s?%s",CLASS_URL_ADS, query);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//format url
	CURL *curl = curl_easy_init();
	char * encodeQuerry = curl_easy_escape(curl,query, 0);
	sprintf(url, "%s?%s",CLASS_URL_ADS, encodeQuerry);
#endif



    //Http request
    std::vector < std::string > header;
    header.push_back(APP_ID);
	header.push_back(REST_API);
    header.push_back("Content-Type: application/json");
    HttpRequest* request = new HttpRequest();
    request->setUrl(url);
    request->setHeaders(header);
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(AdsLayer::callBackGetAdsFromParse,this));
    HttpClient::getInstance()->send(request);
	vt_httpRequest.push_back(request);
}

void AdsLayer::callBackGetAdsFromParse(HttpClient* client, HttpResponse* response)
{
    if (response->isSucceed()) {
    	//Clear data that being got from Parse.com (sometimes stranged characters be attached after the result)
        std::vector<char> *buffer = response->getResponseData();
        const char *data = reinterpret_cast<char *>(&(buffer->front()));
        std::string clearData(data);
        size_t pos = clearData.rfind("}");
        clearData = clearData.substr(0, pos + 1);
        if (clearData =="") return;

        
        
        //Process data
        rapidjson::Document d;
        d.Parse<0>(clearData.c_str());
        const rapidjson::Value& mangJson = d["results"];
        if (clearData.length() > 15) //Data's already been added on Parse.com
        {
            int index = 0;
            adsObjects.gameName = mangJson[index][KEY_WORLD_ADS_GAME_NAME].GetString();
    		const rapidjson::Value& jsonUrl = mangJson[index][KEY_WORLD_ADS_IMAGE];
    		adsObjects.imageLink = jsonUrl["url"].GetString();
            adsObjects.playStoreLink = mangJson[index][KEY_WORLD_ADS_LINK_PLAY_STORE].GetString();
            adsObjects.appStoreLink = mangJson[index][KEY_WORLD_ADS_LINK_APP_STORE].GetString();
            
            getImageFromInternet(adsObjects.imageLink);
        }
        
    }
}


void AdsLayer::getImageFromInternet(string url)
{
    //Http request
    HttpRequest* request = new HttpRequest();
    request->setUrl(url.c_str());
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(AdsLayer::callBackGetImageFromInternet,this));
    HttpClient::getInstance()->send(request);
    vt_httpRequest.push_back(request);
}

void AdsLayer::callBackGetImageFromInternet(HttpClient* client, HttpResponse* response)
{
    if (response->isSucceed()) {


    	std::vector<char> *buffer = response->getResponseData();
		Image* image = new Image();
		if(!image->initWithImageData(reinterpret_cast<unsigned char*>(&(buffer->front())), buffer->size()))
			return;
		textureAds = new Texture2D();
		if(!textureAds->initWithImage(image))
			return;
		if(adsTag == TAG_LOAD_AND_SHOW_ADS)
			showAds();
    }
}
void AdsLayer::loadAds()
{
	adsTag = TAG_LOAD_ADS;

	getAdsFromParse();
}
void AdsLayer::loadAdsAndShow()
{
	adsTag = TAG_LOAD_AND_SHOW_ADS;

	getAdsFromParse();
}
void AdsLayer::showAds()
{

	if(textureAds == NULL)
		loadAdsAndShow();
	else
		addDialog();
}
