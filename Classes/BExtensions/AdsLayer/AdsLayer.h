#ifndef __ADS_LAYER_H__
#define __ADS_LAYER_H__

#include "cocos2d.h"
#include "cocos/ui/CocosGUI.h"
#include "cocos/network/HttpRequest.h"
#include "cocos/network/HttpClient.h"
#include "json/rapidjson.h"
#include "json/document.h"

#include <curl/include/ios/curl/curl.h>
#include <curl/include/android/curl/curl.h>
using namespace cocos2d::network;
using namespace std;
using namespace rapidjson;
using namespace cocos2d::ui;
USING_NS_CC;


class AdsObject
{
public:
	string gameName;
	string imageLink;
	string playStoreLink;
	string appStoreLink;
};
class AdsLayer : public cocos2d::Layer
{
public:

	virtual bool init();
	CREATE_FUNC(AdsLayer);
	~AdsLayer();

	void loadAds();
	void showAds();
private:

	//Components
	int adsTag;
	vector<HttpRequest*> vt_httpRequest; //To release all request in destructor
	Texture2D* textureAds;
	AdsObject adsObjects;

	//Adslayer logic functions
	void addDialog();
	void loadAdsAndShow();
	void onAdsClick();

	//Http functions
	void getAdsFromParse();
	void callBackGetAdsFromParse(HttpClient* client,HttpResponse* response);
	void getImageFromInternet(string url);
	void callBackGetImageFromInternet(HttpClient* client, HttpResponse* response);
};

#endif
