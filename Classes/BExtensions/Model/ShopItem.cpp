
#include "ShopItem.h"
#include "cocos2d.h"
vector<ShopItem*> ShopItem::vt_ShopItems = {};
ShopItem::ShopItem()
{
    
}

ShopItem::~ShopItem()
{
    
}
ShopItem* ShopItem::setAnimationImagesFrom(const rapidjson::Value& json,ShopItem* item) {
		cocos2d::CCLog("bambi sap parse item animation");
	   vector<ItemAnimationImage*> vt_ItemAnim = item->getVt_ItemAnimationImages();

	    ItemAnimationImage* itemAnim = new ItemAnimationImage();
	    itemAnim->setImageIndex(json[KEY_WORLD_ITEM_ANIMATION_INDEX].GetInt());
		const rapidjson::Value& jsonUrl = json[KEY_WORLD_ITEM_IMAGE_URL];
		itemAnim->setImageURL(jsonUrl["url"].GetString());
	    vt_ItemAnim.push_back(itemAnim);

		item->setVt_ItemAnimationImages(vt_ItemAnim);



		cocos2d::CCLog("bambi da parse item animation");
	return item;
}
ShopItem* ShopItem::parseItemFrom(const rapidjson::Value& json) //Parse the data from Parse.com
{
    ShopItem* user=new ShopItem();
	const rapidjson::Value& jsonUrl = json[KEY_WORLD_ITEM_IMAGE_URL];
    user->setImageURL(jsonUrl["url"].GetString());
    user->setName(json[KEY_WORLD_ITEM_NAME].GetString());
    user->setPrice(json[KEY_WORLD_ITEM_PRICE].GetInt());



    return user;
}

