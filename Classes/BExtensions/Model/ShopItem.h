
#ifndef __ShopItem__
#define __ShopItem__
#include <json/writer.h>
#include <json/stringbuffer.h>
#include <json/rapidjson.h>
#include <json/reader.h>
#include <json/writer.h>
#include <json/prettywriter.h>
#include <json/filestream.h>
#include <json/document.h>
#include "EConstants.h"
using namespace rapidjson;
using namespace std;

class ItemAnimationImage{
   CC_SYNTHESIZE(string, _imageURL, ImageURL);
   CC_SYNTHESIZE(int, _imageIndex, ImageIndex);
};
class ShopItem{
public:
    template <typename Writer>  void Serialize(Writer &writer) const {
        writer.StartObject();
        writer.String(KEY_WORLD_ITEM_NAME);
        writer.String(_name.c_str());
        writer.String(KEY_WORLD_ITEM_IMAGE_URL);
        writer.String(_imageURL.c_str());
        writer.String(KEY_WORLD_ITEM_PRICE);
        writer.Int(_price);
        writer.EndObject();
    }
    std::string serialize(){
        StringBuffer s;
        Writer<StringBuffer> writer(s);
        Serialize(writer);
        return  s.GetString();
    }
    ShopItem();
    ~ShopItem();
    CC_SYNTHESIZE(string,_name,Name);
    CC_SYNTHESIZE(string,_imageURL,ImageURL);
    CC_SYNTHESIZE(int,_price,Price);
	CC_SYNTHESIZE(vector<ItemAnimationImage*>, vt_ItemAnimationImages,Vt_ItemAnimationImages);
    static ShopItem* parseItemFrom(const rapidjson::Value& json);
    static ShopItem* setAnimationImagesFrom(const rapidjson::Value& json, ShopItem* item);

    static vector<ShopItem*> vt_ShopItems;
};
#endif
