#ifndef __RANKING_SCENE_H__
#define __RANKING_SCENE_H__

#include "cocos2d.h"
#include "cocos/ui/CocosGUI.h"
#include "FacebookHandler.h"
#include "ParseHandler.h"
#include "BUserInfor.h"
#include "AdsLayer.h"

USING_NS_CC;
using namespace std;
using namespace cocos2d::ui;

class RankingScene: public cocos2d::Layer, public FacebookConnectDelegate, public ParseDelegate{
public:
		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		static cocos2d::Scene* scene();
		cocos2d::Size winSize;
		cocos2d::Point origin;
		CREATE_FUNC(RankingScene)
		void onKeyReleased(EventKeyboard::KeyCode keycode, Event* event);
		void backToHome(Ref* pSender,ui::Widget::TouchEventType eEventType);
		void playAgain(Ref* pSender,ui::Widget::TouchEventType eEventType);



		void worldModeButton(Ref* pSender,ui::Widget::TouchEventType eEventType);
		void friendModeButton(Ref* pSender,ui::Widget::TouchEventType eEventType);
		void facebookConnectButton(Ref* pSender,ui::Widget::TouchEventType eEventType);
		void loadLeaderboardData();
		void showScoreOnScreen(vector<BUserInfor*> listUser);
		void updateUI(int tag);
		void resetRankingScene();


		bool isWorldMode;
		bool isGettingData;
		Sprite* board;
		Button* btnConnectFacebook;
		Button* btnWorldMode;
		Button* btnFriendMode;
		Sprite* loadingSprite;
		Sprite* loadingSprite_child;
		Label* labelError;

		//Responsed function
		virtual void responseWhenLoginOrLogoutFacebook(); //From FacebookHandler
		virtual void responseForQuerryTopWorld(vector<BUserInfor*> worldList); //From ParseHandler
		virtual void responseForQuerryTopFriend(vector<BUserInfor*> worldList); //From ParseHandler
		virtual void responseAfterCheckFacebookIDExistOnParse(); //From ParseHandler
		virtual void responseAfterCheckInstalledGamesOnParse(); //From ParseHandler
	   virtual void responseForQuerryShopItems(vector<ShopItem*> itemList); //From ParseHandler

		//Ads layer
		AdsLayer* adsLayer;
};
#endif
