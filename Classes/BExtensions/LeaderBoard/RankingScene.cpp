#include "RankingScene.h"
#include "SimpleAudioEngine.h"
#include "MenuScene.h"
#include "EResources.h"
#include "CppUtils.h"
#include "GameScene.h"
#include "SonarFrameworks.h"
#include "Resources.h"

using namespace CocosDenshion;

#define TAG_MODE_BUTTON 1231
#define TAG_FACEBOOK_BUTTON 1232
#define TAG_CHILDREN_TO_REMOVED 1233

Scene* RankingScene::scene() {
	// 'scene' is an autorelease object
	Scene *scene = Scene::create();

	// 'layer' is an autorelease object
	RankingScene *layer = RankingScene::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool RankingScene::init() {
	//////////////////////////////
	// 1. super init first
	if (!Layer::init()) {
		return false;
	}
    
    //Hide admob banner
    SonarCocosHelper::AdMob::hideBannerAd(SonarCocosHelper::AdBannerPosition::eBoth);

	origin = Director::getInstance()->getVisibleOrigin();
	winSize = Director::getInstance()->getVisibleSize();
	isGettingData = false;

	//Set delegate to get response from FacebookHandler and ParseHandler
	ParseHandler::getInstance()->setParseDelegate(this);
	FacebookHandler::getInstance()->setFacebookConnectDelegate(this);

	//Set default value for UserDefault
	isWorldMode = UserDefault::getInstance()->getBoolForKey(RANKINGWORLDMODE,true);
	UserDefault::getInstance()->setBoolForKey(RANKINGWORLDMODE, isWorldMode);






	CCLog("bambi sau khi define may bien");



		//Add error label
		TTFConfig configError(s_font_1, 70 * s_font_ratio);
		labelError = Label::createWithTTF(configError,
				"Please check your connection\nand try again later.",
				TextHAlignment::CENTER, winSize.width * 0.7);
		labelError->setPosition(winSize.width / 2, winSize.height * 0.45f);
		labelError->setColor(Color3B(188, 132, 75));
		labelError->setVisible(false);
		this->addChild(labelError, 200);

		CCLog("bambi sau khi define loading sprite");
		//Add background
		auto background = Sprite::create(s_ranking_background);
		background->setPosition(winSize.width / 2, winSize.height / 2);
		background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(background);

		CCLog("bambi sau khi define background");
		//Add paper
		board = Sprite::create(s_ranking_board);
		board->setPosition(winSize.width / 2, winSize.height / 2);
		board->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(board);

		CCLog("bambi sau khi define title");
		//Add title
		auto lTitle = Sprite::create(s_ranking_title);
		lTitle->setPosition(board->getContentSize().width / 2,
				board->getContentSize().height * 0.85f);
		lTitle->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		board->addChild(lTitle);

		CCLog("bambi sau khi define world button");
		//Add worldMode button
		if (!isWorldMode)
			btnWorldMode = Button::create(s_ranking_btnWorldMode_Clicked);
		else
			btnWorldMode = Button::create(s_ranking_btnWorldMode);
		btnWorldMode->setPosition(
				Vec2(board->getContentSize().width * 0.4,
						board->getContentSize().height * 0.7));
		btnWorldMode->setTouchEnabled(true);
		btnWorldMode->setPressedActionEnabled(true);
		btnWorldMode->addTouchEventListener(
				CC_CALLBACK_2(RankingScene::worldModeButton, this));
		board->addChild(btnWorldMode);

		//Add friendMode button
		if (isWorldMode)
			btnFriendMode = Button::create(s_ranking_btnFriendMode);
		else
			btnFriendMode = Button::create(s_ranking_btnFriendMode_CLicked);
		btnFriendMode->setPosition(
				Vec2(board->getContentSize().width * 0.6,
						board->getContentSize().height * 0.7));
		btnFriendMode->setTouchEnabled(true);
		btnFriendMode->setPressedActionEnabled(true);
		btnFriendMode->addTouchEventListener(
				CC_CALLBACK_2(RankingScene::friendModeButton, this));
		board->addChild(btnFriendMode);

		//Add connectFacebook button
		btnConnectFacebook = Button::create(s_ranking_btnConnectFacebook_LogIn);
		btnConnectFacebook->setPosition(Vec2(winSize.width-5, winSize.height-5));
		btnConnectFacebook->setTouchEnabled(true);
		btnConnectFacebook->setPressedActionEnabled(true);
		btnConnectFacebook->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
		btnConnectFacebook->addTouchEventListener(
				CC_CALLBACK_2(RankingScene::facebookConnectButton, this));
		this->addChild(btnConnectFacebook);

		CCLog("bambi sau khi facebook connect button");
		//Add home button
		auto btn_home = Button::create(s_ranking_btnHome);
		btn_home->setPosition(
				Vec2(winSize.width-4,
						winSize.height - btnConnectFacebook->getContentSize().height
								- 10));
		btn_home->setTouchEnabled(true);
		btn_home->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
		btn_home->addTouchEventListener(
				CC_CALLBACK_2(RankingScene::backToHome, this));
		this->addChild(btn_home);

		CCLog("bambi sau khi define home button");
		//Add play again button
			auto btn_replay = Button::create(s_ranking_btnPlay);
			btn_replay->setPosition(
					Vec2(winSize.width-4,
							winSize.height - btnConnectFacebook->getContentSize().height - 10 - btn_home->getContentSize().height
									- 10));
			btn_replay->setTouchEnabled(true);
			btn_replay->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
			btn_replay->addTouchEventListener(
					CC_CALLBACK_2(RankingScene::playAgain, this));
			this->addChild(btn_replay);

			CCLog("bambi sau khi define play button");

			//Update all button image
			updateUI(TAG_MODE_BUTTON);
			updateUI(TAG_FACEBOOK_BUTTON);




			//Add loading Sprite
				loadingSprite = Sprite::create(s_ranking_loadingSprite);
				loadingSprite->setPosition(winSize.width / 2, winSize.height * 0.45f);
				auto rotation = RotateBy::create(0.25f, 60);
				loadingSprite->runAction(RepeatForever::create(rotation));
				this->addChild(loadingSprite, 200);
				loadingSprite_child = Sprite::create(s_ranking_loadingSpriteChild);
				loadingSprite_child->setPosition(winSize.width / 2, winSize.height * 0.45f);
				this->addChild(loadingSprite_child, 200);

			CCLog("bambi sau khi update ui");
		    //After load check facebook on parse responseAfterCheckFacebookIDExistOnParse function will be called
			if(FacebookHandler::getInstance()->isFacebookLoggedIn())
				ParseHandler::getInstance()->checkFacebookIdExistOnParse();
			else
			{
				loadingSprite->setVisible(false);
				loadingSprite_child->setVisible(false);
				labelError->setString("Please login to facebook.");
				labelError->setVisible(true);
			}

			//Load ads layer
//			    adsLayer = AdsLayer::create();
//				this->addChild(adsLayer,999);
//				adsLayer->showAds();










	//Keyboard handling
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased =
			CC_CALLBACK_2(RankingScene::onKeyReleased,this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener,
			this);

	return true;
}

void RankingScene::facebookConnectButton(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
		if (FacebookHandler::getInstance()->isFacebookLoggedIn())
			FacebookHandler::getInstance()->logoutFacebook();
		else
			FacebookHandler::getInstance()->loginFacebook();
		//After login or logout, responseWhenLoginOrLogoutFacebook will be called
	}
}
void RankingScene::worldModeButton(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED && !isWorldMode && !isGettingData) {
			UserDefault::getInstance()->setBoolForKey(RANKINGWORLDMODE, true);
			isWorldMode = true;
			updateUI(TAG_MODE_BUTTON);
			loadLeaderboardData();
	}
}
void RankingScene::friendModeButton(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED && isWorldMode && !isGettingData) {
			UserDefault::getInstance()->setBoolForKey(RANKINGWORLDMODE, false);
			isWorldMode = false;
			updateUI(TAG_MODE_BUTTON);
			loadLeaderboardData();
	}
}
void RankingScene::updateUI(int tag)
{
	switch (tag) {
		case TAG_MODE_BUTTON:
		{
			CCLog("bambi update ui vao mode button");
			//Update image of mode button
			if (isWorldMode)
			{CCLog("bambi update ui vao mode button : true");
				btnWorldMode->loadTextureNormal(s_ranking_btnWorldMode_Clicked);
				btnFriendMode->loadTextureNormal(s_ranking_btnFriendMode);
			}
			else
			{
				CCLog("bambi update ui vao mode button: false");
				btnWorldMode->loadTextureNormal(s_ranking_btnWorldMode);
				btnFriendMode->loadTextureNormal(s_ranking_btnFriendMode_CLicked);
			}
		}
			break;
		case TAG_FACEBOOK_BUTTON:
		{
			CCLog("bambi update ui vao facebook button");
			//Update image of facebook button
			if (FacebookHandler::getInstance()->isFacebookLoggedIn())
			{
				CCLog("bambi update ui vao facebook button da login");
				btnConnectFacebook->loadTextureNormal(s_ranking_btnConnectFacebook_LogOut);
			}
			else
			{
				CCLog("bambi update ui vao facebook button chua login");
				btnConnectFacebook->loadTextureNormal(s_ranking_btnConnectFacebook_LogIn);
			}
		}
			break;
	}
}

void RankingScene::responseWhenLoginOrLogoutFacebook()
{
	updateUI(TAG_FACEBOOK_BUTTON);
	//After load check facebook on parse responseAfterCheckFacebookIDExistOnParse function will be called
	if(FacebookHandler::getInstance()->isFacebookLoggedIn())
		ParseHandler::getInstance()->checkFacebookIdExistOnParse();
	else
		resetRankingScene(); //Reset ranking scene.
}
void RankingScene::responseAfterCheckFacebookIDExistOnParse()
{
	loadLeaderboardData();
}
void RankingScene::loadLeaderboardData()
{
	auto vt_BoardChildren = board->getChildren();
	for(Node* child : vt_BoardChildren)
		if(child->getTag() == TAG_CHILDREN_TO_REMOVED)
			child->removeFromParent();


	if(FacebookHandler::getInstance()->isFacebookLoggedIn())
	{
		isGettingData = true;
		labelError->setVisible(false);
		loadingSprite->setVisible(true);
		loadingSprite_child->setVisible(true);
		if(isWorldMode)
			ParseHandler::getInstance()->fetchTopWorld();
		else
			ParseHandler::getInstance()->fetchTopFriend();
		//After fetch data responseForQuerryTopWorld or responseForQuerryTopFriend will be called.
	}else
	{
		labelError->setVisible(true);
		isGettingData = false;
	}
}
void RankingScene::responseForQuerryTopWorld(vector<BUserInfor*> worldList)
{
	CCLog("bambi ranking scene roi ne");
	//Check installed games, if it the first time user login into this game, +100 coins
	bool isFirstTimeLoginIntoThisGame = UserDefault::getInstance()->getBoolForKey(KEY_IS_FIRST_TIME_LOGIN_INTO_THIS_GAME,true);
	if(isFirstTimeLoginIntoThisGame)
		ParseHandler::getInstance()->checkInstalledGames();
	else
		showScoreOnScreen(worldList);
}
void RankingScene::responseForQuerryTopFriend(vector<BUserInfor*> friendList)
{
	//Check installed games, if it the first time user login into this game, +100 coins
	bool isFirstTimeLoginIntoThisGame = UserDefault::getInstance()->getBoolForKey(KEY_IS_FIRST_TIME_LOGIN_INTO_THIS_GAME,true);
	if(isFirstTimeLoginIntoThisGame)
		ParseHandler::getInstance()->checkInstalledGames();
	else
		showScoreOnScreen(friendList);
}
void RankingScene::responseAfterCheckInstalledGamesOnParse()
{
	//Reset ranking scene.
	resetRankingScene();

}
void RankingScene::resetRankingScene()
{
	auto *newScene = RankingScene::scene();
			 Director *pDirector = Director::getInstance();
			 pDirector->replaceScene(newScene);
}
void RankingScene::showScoreOnScreen(vector<BUserInfor*> listUser) {

	loadingSprite->setVisible(false);
	loadingSprite_child->setVisible(false);
	isGettingData = false;

	TTFConfig configUser(s_font, 60);
	float positionYBegin = board->getContentSize().height * 0.6;
	float eachUserMargin = board->getContentSize().height * 0.12;
	for (int i = 0; i < listUser.size(); i++) {
		//Background and label of rank (at the left side)
		Sprite* numberBackgroundSprite;
		numberBackgroundSprite = Sprite::create(s_ranking_number);
		numberBackgroundSprite->setPosition(board->getContentSize().width * 0.15f,positionYBegin - eachUserMargin * i);
		numberBackgroundSprite->setTag(TAG_CHILDREN_TO_REMOVED);
		board->addChild(numberBackgroundSprite);

		auto lRanking = Label::createWithTTF(configUser,
				CppUtils::doubleToString(i + 1), TextHAlignment::CENTER);
		lRanking->setPosition(numberBackgroundSprite->getContentSize().width/2,numberBackgroundSprite->getContentSize().height /2);
		lRanking->setColor(Color3B(255,255,255));
		numberBackgroundSprite->addChild(lRanking);


		//Split name if it's exessive and add label Name add the middle
		string stringName = listUser[i]->getName();
		if(stringName.length() > 12)
		{
			auto vt_splitedString = CppUtils::splitString(listUser[i]->getName());
			stringName = vt_splitedString[vt_splitedString.size()-1];
		}
		auto lName = Label::createWithTTF(configUser, stringName,TextHAlignment::CENTER);
		lName->setPosition(board->getContentSize().width * 0.5f,positionYBegin - eachUserMargin * i);
		lName->setColor(Color3B(255,255,255));
		lName->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		lName->setTag(TAG_CHILDREN_TO_REMOVED);
		board->addChild(lName);


		//Label score (at the right side)
		auto lScore = Label::createWithTTF(configUser,
				CppUtils::doubleToString(listUser[i]->getScore()),
				TextHAlignment::CENTER);
		lScore->setPosition(board->getContentSize().width * 0.85f,positionYBegin - eachUserMargin * i);
		lScore->setColor(Color3B(255,255,255));
		lScore->setTag(TAG_CHILDREN_TO_REMOVED);
		board->addChild(lScore);
	}
}














void RankingScene::onKeyReleased(EventKeyboard::KeyCode keycode, Event* event) {

	if (EventKeyboard::KeyCode::KEY_ESCAPE == keycode) {
        auto *newScene = GameScene::scene();
        auto transition = TransitionFade::create(1.0, newScene);
        Director *pDirector = Director::getInstance();
        pDirector->replaceScene(transition);
	}
}
void RankingScene::backToHome(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
		auto *newScene = MenuScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
	}
}
void RankingScene::playAgain(Ref* pSender,
		ui::Widget::TouchEventType eEventType) {
	if (eEventType == ui::Widget::TouchEventType::ENDED) {
		auto *newScene = GameScene::scene();
		auto transition = TransitionFade::create(1.0, newScene);
		Director *pDirector = Director::getInstance();
		pDirector->replaceScene(transition);
	}
}
void RankingScene::responseForQuerryShopItems(vector<ShopItem*> itemList){
}


