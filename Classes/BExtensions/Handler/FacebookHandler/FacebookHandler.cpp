
#include "FacebookHandler.h"
#include "EConstants.h"
#include "BUserInfor.h"
#include "ParseHandler.h"
#include "NDKHelper.h"

FacebookHandler::FacebookHandler()
{

}

FacebookHandler::~FacebookHandler()
{
    
}
void FacebookHandler::shareFacebook()
{
	sdkbox::FBShareInfo info;
	info.type = sdkbox::FB_LINK;
	info.link = "http://www.boredninjas.com/springninja.html";
	info.title = "Spring Ninja";
	info.text = "It's a great game from Bored Ninjas you must try!";
	info.image = "http://www.boredninjas.com/springninja.png";
	sdkbox::PluginFacebook::dialog(info);
}
FacebookHandler* FacebookHandler::getInstance()
{
    static FacebookHandler* instance;
    if (instance==nullptr) {
        instance=new FacebookHandler();
        sdkbox::PluginFacebook::setListener(instance);
    }
    return instance;
}
string FacebookHandler::getUserFacebookID()
{
	return sdkbox::PluginFacebook::getUserID();
}
void FacebookHandler::getAllFriendsID()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sdkbox::PluginFacebook::FBAPIParam params;
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	sdkbox::FBAPIParam params;
#endif
	sdkbox::PluginFacebook::fetchFriends();
}

void FacebookHandler::loginFacebook()
{
	sdkbox::PluginFacebook::requestReadPermissions({"public_profile", "user_friends"}); //Including login
}
void FacebookHandler::logoutFacebook()
{
	sdkbox::PluginFacebook::logout();
	if(_facebookConnectDelegate != nullptr)
		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
bool FacebookHandler::isFacebookLoggedIn()
{
	if (sdkbox::PluginFacebook::isLoggedIn())
		return true;
	else
		return false;
}
void FacebookHandler::getMyProfile()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sdkbox::PluginFacebook::FBAPIParam params;
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	sdkbox::FBAPIParam params;
#endif
	params["fields"] = "name,id,locale";
	sdkbox::PluginFacebook::api("/me", "GET", params, "/me");
}


/*********************
 * Facebook callbacks
 *********************/
void FacebookHandler::onLogin(bool isLogin, const std::string& error)
{
	if(_facebookConnectDelegate != nullptr && isLogin)
		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
void FacebookHandler::onAPI(const std::string& tag, const std::string& jsonData)
{
//    CCLog("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());
   if(tag == "/me")
   {
	  BUserInfor* user=BUserInfor::parseUserFrom(jsonData);
	  user->setScore(0);

	  if(_facebookDelegate != nullptr)
		_facebookDelegate->responseWhenGetMyInfoSuccessfully(user);
   }
}
void FacebookHandler::onSharedSuccess(const std::string& message)
{
	//Show toast
	CCDictionary* prms = CCDictionary::create();
	prms->setObject(CCString::create("Got 100 free coins!"), "messageToShow");
	SendMessageWithParams(string("showToast"), prms);

	//Submit score
	ParseHandler::getInstance()->submitScore(100);
}

void FacebookHandler::onSharedCancel()
{
	//Show toast
	CCDictionary* prms = CCDictionary::create();
	prms->setObject(CCString::create("Share Facebook to get 100 free coins!"), "messageToShow");
	SendMessageWithParams(string("showToast"), prms);
}

void FacebookHandler::onSharedFailed(const std::string& message)
{
}
void FacebookHandler::onPermission(bool isLogin, const std::string& error)
{
    if(_facebookConnectDelegate != nullptr && isLogin)
    		_facebookConnectDelegate->responseWhenLoginOrLogoutFacebook();
}
void FacebookHandler::onFetchFriends(bool ok, const std::string& msg)
{
//    CCLog("##FB %s: %d = %s", __FUNCTION__, ok, msg.data());
    string friendList="";
    const std::vector<sdkbox::FBGraphUser>& friends = sdkbox::PluginFacebook::getFriends();
    for (int i = 0; i < friends.size(); i++)
	{
		const sdkbox::FBGraphUser& user = friends.at(i);
		friendList=friendList+"\""+user.uid.data()+"\",";
	}
    
    if(_facebookDelegate != nullptr)
    		_facebookDelegate->responseWhenGetFriendsSuccessfully(friendList);
}
