#include "ParseHandler.h"
#include "EConstants.h"
#include <curl/include/ios/curl/curl.h>
#include <json/rapidjson.h>
#include <json/reader.h>
#include <json/writer.h>
#include <json/prettywriter.h>
#include <json/filestream.h>
#include <json/document.h>
#include <json/stringbuffer.h>

ParseHandler::ParseHandler() {
	isSubmitingInstalledGame = false;
	scoreToSubmit = 0;
	FacebookHandler::getInstance()->setFacebookDelegate(this);
}

ParseHandler::~ParseHandler() {

}

ParseHandler* ParseHandler::getInstance() {
	static ParseHandler* instance;
	if (!instance) {
		instance = new ParseHandler();
	}
	return instance;
}

//Shop
void ParseHandler::getAvailableItemsOnServer() {
	ShopItem::vt_ShopItems.clear();

	//query string
	char query[200];
	sprintf(query, "where={\"%s\":true,\"%s\":\"%s\"}", KEY_WORLD_ITEM_IS_AVAILABLE,KEY_WORLD_ADS_GAME_NAME,THIS_GAME_NAME);
	char url[55500];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	sprintf(url, "%s?%s&order=ItemPrice",CLASS_URL_SHOP, query);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//format url
	CURL *curl = curl_easy_init();
	char * encodeQuerry = curl_easy_escape(curl,query, 0);
	sprintf(url, "%s?%s&order=ItemPrice",CLASS_URL_SHOP, encodeQuerry);
#endif
	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(
			CC_CALLBACK_2(ParseHandler::getAvailableItemsOnServerCallback,this));
	HttpClient::getInstance()->send(request);
	request->release();
}

int numberOfItemsNeedToParse = 0;
void ParseHandler::getAvailableItemsOnServerCallback(HttpClient* client,
		HttpResponse* response) {
	if (response->isSucceed()) {
		//Clear data (sometimes stranged characters be attached after the result)
		std::vector<char> *buffer = response->getResponseData();
		const char *data = reinterpret_cast<char *>(&(buffer->front()));
		std::string clearData(data);
		size_t pos = clearData.rfind("}");
		clearData = clearData.substr(0, pos + 1);
		if (clearData == "")
			return;

		CCLog("bambi parse callback: %s",clearData.c_str());
		//Process data
		rapidjson::Document d;
		d.Parse<0>(clearData.c_str());
		const rapidjson::Value& jsonArray = d["results"];
		numberOfItemsNeedToParse = jsonArray.Size();
 		for (int k = 0; k < jsonArray.Size(); k++)
 		{
 			ShopItem* item = ShopItem::parseItemFrom(jsonArray[k]);
			ShopItem::vt_ShopItems.push_back(item);

			//Get animation images from server
			getAnimationImagesFromServer(item->getName());
 		}

	}
}


void ParseHandler::getAnimationImagesFromServer(string itemName) {
	//Querry
	char querry[55500];
    sprintf(querry, "where={\"%s\":\"%s\",\"%s\":\"%s\"}",KEY_WORLD_ITEM_NAME,itemName.c_str(),KEY_WORLD_ADS_GAME_NAME,THIS_GAME_NAME);


    //Format url
    char url[55500];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sprintf(url, "%s?%s&order=-createAt",CLASS_URL_ANIMATION_IMAGES, querry);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    //format url
    CURL *curl = curl_easy_init();
    char * encodeQuerry = curl_easy_escape(curl,querry, 0);
    sprintf(url, "%s?%s&order=-createAt",CLASS_URL_ANIMATION_IMAGES, encodeQuerry);
#endif

	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(ParseHandler::getAnimationImagesFromServerCallback,this));
	request->setTag(itemName.c_str());
	HttpClient::getInstance()->send(request);
	request->release();
}
void ParseHandler::getAnimationImagesFromServerCallback(HttpClient* client,
		HttpResponse* response) {
	std::string error = response->getErrorBuffer();
		if (response->isSucceed() && error == "") {

			//Clear data that being got from Parse.com (sometimes stranged characters be attached after the result)
			std::vector<char> *buffer = response->getResponseData();
			const char *data = reinterpret_cast<char *>(&(buffer->front()));
			std::string clearData(data);
			size_t pos = clearData.rfind("}");
			clearData = clearData.substr(0, pos + 1);
			if (clearData == "")
				return;

			//Process data
			rapidjson::Document document;
			document.Parse<0>(clearData.c_str());
			if (!document["results"].IsArray() || clearData.length()<15)
				return;

			CCLog("bambi json:%s",clearData.c_str());

			const rapidjson::Value& jsonArray = document["results"];
			for (int k = 0; k < jsonArray.Size(); k++) {
				CCLog("vao vong for ne");
				ShopItem* itemToGetAnimationImages = ShopItem::vt_ShopItems[0];
				for(ShopItem* item : ShopItem::vt_ShopItems)
					if(item->getName() == response->getHttpRequest()->getTag())
						itemToGetAnimationImages = item;
				itemToGetAnimationImages = ShopItem::setAnimationImagesFrom(jsonArray[k],itemToGetAnimationImages);
			}

			CCLog("bambi numberofitemneedtoparse:%d",numberOfItemsNeedToParse);
			numberOfItemsNeedToParse--;
			if(numberOfItemsNeedToParse <= 0)
			{
				//Response to shop scene
				if(_parseDelegate != nullptr)
					_parseDelegate->responseForQuerryShopItems();
			}
		}
}




















//Leaderboard
void ParseHandler::checkInstalledGames() {
	//query string
	char query[200];
	sprintf(query, "where={\"%s\":\"%s\"}", KEY_WORLD_ID,
			FacebookHandler::getInstance()->getUserFacebookID().c_str());


	CCLog("bambi check installed games ne,fbid:%s",
			FacebookHandler::getInstance()->getUserFacebookID().c_str());


   	char url[55500];
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	sprintf(url, "%s?%s", CLASS_URL_USER, query);
    #endif
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    	//format url
    	CURL *curl = curl_easy_init();
    	char * encodeQuerry = curl_easy_escape(curl,query, 0);
    	sprintf(url, "%s?%s", CLASS_URL_USER, encodeQuerry);
    #endif






	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(
			CC_CALLBACK_2(ParseHandler::checkInstalledGameCallback,this));
	HttpClient::getInstance()->send(request);
	request->release();
}
void ParseHandler::checkInstalledGameCallback(HttpClient* client,
		HttpResponse* response) {
	CCLog("bambi checkInstalledGameCallback ne");
	if (response->isSucceed()) {
		//Clear data (sometimes stranged characters be attached after the result)
		std::vector<char> *buffer = response->getResponseData();
		const char *data = reinterpret_cast<char *>(&(buffer->front()));
		std::string clearData(data);
		size_t pos = clearData.rfind("}");
		clearData = clearData.substr(0, pos + 1);
		if (clearData == "")
			return;


		CCLog("bambi checkInstalledGameCallback success:%s",clearData.c_str());
		//Process data
		rapidjson::Document d;
		d.Parse<0>(clearData.c_str());
		const rapidjson::Value& mangJson = d["results"];
		if (clearData.length() > 15) //There is the result available.
				{
			string installedGameString =
					mangJson[0][KEY_WORLD_INSTALLED_GAMES].GetString();
			if (installedGameString.find(THIS_GAME_NAME) == std::string::npos) //If it's the first time to use this app, give user 100 coins
				submitInstalledGame(installedGameString + "," + THIS_GAME_NAME);
			else
			{
				if(ParseHandler::getInstance()->getParseDelegate() != nullptr)
					ParseHandler::getInstance()->getParseDelegate()->responseAfterCheckInstalledGamesOnParse();
				UserDefault::getInstance()->setBoolForKey(KEY_IS_FIRST_TIME_LOGIN_INTO_THIS_GAME,false);
			}
		}
	}
}

void ParseHandler::submitInstalledGame(string gameName) {
	isSubmitingInstalledGame = true;

	//Set url
	char url[55500];
	sprintf(url, "%s/%s", CLASS_URL_USER,
			UserDefault::getInstance()->getStringForKey(KEY_WORLD_OJECTID, "").c_str());

	//Set score data
	char data[100];
	sprintf(data, "{\"%s\":\"%s\"}", KEY_WORLD_INSTALLED_GAMES,
			gameName.c_str());
	string dataStr(data);

	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestData(dataStr.c_str(), dataStr.size());
	request->setRequestType(HttpRequest::Type::PUT);
	HttpClient::getInstance()->send(request);
	request->release();
	request->setResponseCallback(
			[this](HttpClient* client,HttpResponse* response) {
				if (response->isSucceed())
				submitScore(100); //Give user 100 coins if submit installed successfully
			});
}
void ParseHandler::checkFacebookIdExistOnParse() {
	CCLog("bambi checkFacebookIdExistOnParse");
	//query string
	char query[200];
	sprintf(query, "where={\"%s\":\"%s\"}", KEY_WORLD_ID,
			FacebookHandler::getInstance()->getUserFacebookID().c_str());

   	char url[55500];
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	sprintf(url, "%s?%s", CLASS_URL_USER, query);
    #endif
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    	//format url
    	CURL *curl = curl_easy_init();
    	char * encodeQuerry = curl_easy_escape(curl,query, 0);
    	sprintf(url, "%s?%s", CLASS_URL_USER, encodeQuerry);
    #endif




	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(
			CC_CALLBACK_2(ParseHandler::checkFacebookIdExistOnParseCallBack,this));
	HttpClient::getInstance()->send(request);
	request->release();

}
void ParseHandler::checkFacebookIdExistOnParseCallBack(HttpClient* client,
		HttpResponse* response) {
	if (response->isSucceed()) {
		//Clear data (sometimes stranged characters be attached after the result)
		std::vector<char> *buffer = response->getResponseData();
		const char *data = reinterpret_cast<char *>(&(buffer->front()));
		std::string clearData(data);
		size_t pos = clearData.rfind("}");
		clearData = clearData.substr(0, pos + 1);
		if (clearData == "")
			return;
		CCLog("bambi checkFacebookIdExistOnParse:%s",clearData.c_str());
		//Process data
		rapidjson::Document d;
		d.Parse<0>(clearData.c_str());
		const rapidjson::Value& mangJson = d["results"];
		if (clearData.length() > 15) //User's already been added on Parse.com
		{
			char* objectId = (char*) mangJson[0][KEY_WORLD_OJECTID].GetString();
			//Save to UserDefault (your will need objectId when posting score on Parse.com)
			UserDefault::getInstance()->setStringForKey(KEY_WORLD_OJECTID,
					objectId);
			if (_parseDelegate != nullptr)
				_parseDelegate->responseAfterCheckFacebookIDExistOnParse(); //Respone to ranking scene
		} else
			FacebookHandler::getInstance()->getMyProfile();
			//After getMyProfile, responseWhenGetMyInfoSuccessfully function will be called.

	}
}
void ParseHandler::responseWhenGetMyInfoSuccessfully(BUserInfor* user) {
	saveFacebookIdOnParse(user);
}
void ParseHandler::saveFacebookIdOnParse(BUserInfor* user) {
	char url[55500];
	sprintf(url, "%s", CLASS_URL_USER);

	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestData(user->serialize().c_str(),
			user->serialize().size());
	request->setRequestType(HttpRequest::Type::POST);
	request->setResponseCallback(
			CC_CALLBACK_2(ParseHandler::callBacksaveFacebookIdOnParse,this));
	HttpClient::getInstance()->send(request);
	request->release();
}

void ParseHandler::callBacksaveFacebookIdOnParse(HttpClient* client,
		HttpResponse* response) {
	if (response->isSucceed()) {
		//Clear data (sometimes stranged characters be attached after the result)
		std::vector<char> *buffer = response->getResponseData();
		const char *data = reinterpret_cast<char *>(&(buffer->front()));
		std::string clearData(data);
		size_t pos = clearData.rfind("}");
		clearData = clearData.substr(0, pos + 1);
		if (clearData == "")
			return;

		//Process data
		rapidjson::Document document;
		document.Parse<0>(clearData.c_str());
		const rapidjson::Value& jsonArray = document;
		char* objectId = (char*) jsonArray[KEY_WORLD_OJECTID].GetString();
		//Save to UserDefault (your will need objectId when posting score on Parse.com)
		UserDefault::getInstance()->setStringForKey(KEY_WORLD_OJECTID,
				objectId);
		if (_parseDelegate != nullptr)
			_parseDelegate->responseAfterCheckFacebookIDExistOnParse(); //Respone to ranking scene

	}
}

void ParseHandler::fetchBUserInforAt(char* querry) {
	CCLog("bambi fetchBUserInforAt:%s",querry);

	char url[55500];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	sprintf(url, "%s?%s&order=-Score&limit=5",CLASS_URL_USER, querry);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//format url
	CURL *curl = curl_easy_init();
	char * encodeQuerry = curl_easy_escape(curl,querry, 0);
	sprintf(url, "%s?%s&order=-Score&limit=5",CLASS_URL_USER, encodeQuerry);
#endif

	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(
			CC_CALLBACK_2(ParseHandler::callBackFetchBUserInforAt,this));
	HttpClient::getInstance()->send(request);
	request->release();
}

void ParseHandler::callBackFetchBUserInforAt(HttpClient* client,
		HttpResponse* response) {
	CCLog("bambi callBackFetchBUserInforAt");

	std::string error = response->getErrorBuffer();
	if (response->isSucceed() && error == "") {
		//Clear old data
		_friendList.clear();
		_worldList.clear();

		//Clear data that being got from Parse.com (sometimes stranged characters be attached after the result)
		std::vector<char> *buffer = response->getResponseData();
		const char *data = reinterpret_cast<char *>(&(buffer->front()));
		std::string clearData(data);
		size_t pos = clearData.rfind("}");
		clearData = clearData.substr(0, pos + 1);
		if (clearData == "")
			return;


		CCLog("bambi sucess:%s",clearData.c_str());
		//Process data
		rapidjson::Document document;
		document.Parse<0>(clearData.c_str());
		if (!document["results"].IsArray())
			return;
		const rapidjson::Value& jsonArray = document["results"];
		for (int k = 0; k < jsonArray.Size(); k++) {
			BUserInfor* user = BUserInfor::parseUserFrom(jsonArray[k]);
			switch (tag) {
			case TAG_FRIEND:
				_friendList.push_back(user);
				break;
			case TAG_WORLD:
				_worldList.push_back(user);
				break;
			}
		}
	}
	CCLog("bambi sap tra ra ranking roi ne");
	// Response to RankingScene
	if (!_parseDelegate)
		return;

	CCLog("bambi parsedelegate ok roi");
	switch (tag) {
	case TAG_FRIEND:
		_parseDelegate->responseForQuerryTopFriend(_friendList);
		break;
	case TAG_WORLD:
		_parseDelegate->responseForQuerryTopWorld(_worldList);
		break;
	}
}

void ParseHandler::fetchTopFriend() {
	FacebookHandler::getInstance()->getAllFriendsID();
	//After get friends successfully responseWhenGetFriendsSuccessfully will be called.
}
void ParseHandler::responseWhenGetFriendsSuccessfully(string friendList) {
	CCLog("bambi responseWhenGetFriendsSuccessfully");
	friendList += "\"" + FacebookHandler::getInstance()->getUserFacebookID()
			+ "\""; //Attach my FacebookID to friendList

	char querry[55500];
	tag = TAG_FRIEND;
	sprintf(querry, "where={\"FB_ID\":{\"$in\":[%s]}}", friendList.c_str());
	fetchBUserInforAt(querry);
}
void ParseHandler::fetchTopWorld() {
	tag = TAG_WORLD;
	fetchBUserInforAt("");
}

//Submit score
void ParseHandler::fetchScoreFromServer() {
	//query string
	char query[200];
	sprintf(query, "where={\"%s\":\"%s\"}", KEY_WORLD_ID,
			FacebookHandler::getInstance()->getUserFacebookID().c_str());

	char url[55500];
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	sprintf(url, "%s?%s", CLASS_URL_USER, query);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//format url
	CURL *curl = curl_easy_init();
	char * encodeQuerry = curl_easy_escape(curl,query, 0);
	sprintf(url, "%s?%s", CLASS_URL_USER, encodeQuerry);
#endif




	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback([this](HttpClient* client,
			HttpResponse* response) {
		if (response->isSucceed()) {
			//Clear data (sometimes stranged characters be attached after the result)
			std::vector<char> *buffer = response->getResponseData();
			const char *data = reinterpret_cast<char *>(&(buffer->front()));
			std::string clearData(data);
			size_t pos = clearData.rfind("}");
			clearData = clearData.substr(0, pos + 1);
			if (clearData == "")
			return;

			//Process data
			rapidjson::Document d;
			d.Parse<0>(clearData.c_str());
			const rapidjson::Value& mangJson = d["results"];
			if (clearData.length() > 15)//User's already been added on Parse.com
			{
				int score = scoreToSubmit + mangJson[0][KEY_WORLD_SCORE].GetInt();
                
                CCLOG("FetchScoreFromServer: %d", score);
                
				putScoreToSever(score);
			}

		}
	});
	HttpClient::getInstance()->send(request);
	request->release();
}
void ParseHandler::putScoreToSever(int score) {
	//Set url
	char url[55500];
	sprintf(url, "%s/%s", CLASS_URL_USER,
			UserDefault::getInstance()->getStringForKey(KEY_WORLD_OJECTID, "").c_str());

	//Set score data
	char data[100];
	sprintf(data, "{\"%s\":%d}", KEY_WORLD_SCORE, score);
	string dataStr(data);

	//Header for httprequest
	std::vector < std::string > header;
	header.push_back(APP_ID);
	header.push_back(REST_API);
	header.push_back("Content-Type: application/json");

	//Request http
	HttpRequest* request = new HttpRequest();
	request->setUrl(url);
	request->setHeaders(header);
	request->setRequestData(dataStr.c_str(), dataStr.size());
	request->setRequestType(HttpRequest::Type::PUT);
	HttpClient::getInstance()->send(request);
	request->release();
	if (isSubmitingInstalledGame)
		request->setResponseCallback(
				[=](HttpClient* client,HttpResponse* response) {
					isSubmitingInstalledGame = false;
					if(ParseHandler::getInstance()->getParseDelegate() != nullptr)
					ParseHandler::getInstance()->getParseDelegate()->responseAfterCheckInstalledGamesOnParse();
					if (response->isSucceed())
					UserDefault::getInstance()->setBoolForKey(KEY_IS_FIRST_TIME_LOGIN_INTO_THIS_GAME,false);
				});

}
void ParseHandler::submitScore(int score) {
	scoreToSubmit = score;
	fetchScoreFromServer();
}

void ParseHandler::isInternetConnect(){
    
	 CCLog("bambi isinternect check ne");
	    HttpRequest *r = new HttpRequest();
	    r->setUrl("https://google.com");
	    r->setRequestType(HttpRequest::Type::GET);
	    r->setResponseCallback(
	                           [=](network::HttpClient *client, network::HttpResponse *response){

	        CCLog("bambi isinternect check response ne ne");
	                               if (response->isSucceed()) {

	                                   if (_parseDelegate != nullptr)
	                                       _parseDelegate->responseAfterFetchInternetStatus(true) ;


	                               }
	                               else{

	                                   if (_parseDelegate != nullptr)
	                                       _parseDelegate->responseAfterFetchInternetStatus(false) ;


	                               }
	                           }
	                           );
	    HttpClient::getInstance()->send(r);
	    r->release();
    
}

void ParseHandler::fetchScoreFromParse(){
        
        string userFacebookID = FacebookHandler::getInstance()->getUserFacebookID().c_str();
        
        if(userFacebookID != ""){
            //Query String
            char query[200];
            sprintf(query, "where={\"%s\":\"%s\"}",KEY_WORLD_ID,FacebookHandler::getInstance()->getUserFacebookID().c_str());
            

        	char url[55500];
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        	sprintf(url, "%s?%s", CLASS_URL_USER, query);
        #endif
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        	//format url
        	CURL *curl = curl_easy_init();
        	char * encodeQuerry = curl_easy_escape(curl,query, 0);
        	sprintf(url, "%s?%s", CLASS_URL_USER, encodeQuerry);
        #endif



            
            //Header For HTTPRequest
            std::vector< std::string > header;
            header.push_back(APP_ID);
            header.push_back(REST_API);
            header.push_back("Content-Type: application/json");
            
            //Request HTTP
            HttpRequest *request = new HttpRequest();
            request->setUrl(url);
            request->setHeaders(header);
            request->setRequestType(HttpRequest::Type::GET);
            request->setResponseCallback([this](HttpClient *client, HttpResponse *response){
                int score = 0;
                if (response->isSucceed()) {
                    std::vector<char> *buffer = response->getResponseData();
                    const char *data = reinterpret_cast<char *>(&(buffer->front()));
                    std::string clearData(data);
                    size_t pos = clearData.rfind("}");
                    clearData = clearData.substr(0, pos + 1);
                    if (clearData == "") {
                        return ;
                    }
                    //process data
                    rapidjson::Document d;
                    d.Parse<0>(clearData.c_str());
                    const rapidjson::Value &mangJson = d["results"];
                    if (clearData.length() > 15) {
                        
                        //score = scoreToSubmit + mangJson[0][KEY_WORLD_SCORE].GetInt();
                        
                        //Diem Moi
                        score = mangJson[0][KEY_WORLD_SCORE].GetInt();
                        CCLOG("FetchScoreFromParseBySteven: %d", score);
                    }
                }
                if (score > 0 && _parseDelegate != nullptr) {
                    _parseDelegate->responseAfterGetCoinsFromParse( score );
                }
            });
            HttpClient::getInstance()->send(request);
            request->release();
        }
    
}
