
#ifndef __ParseHandler__
#define __ParseHandler__
#include <cocos/network/HttpClient.h>
#include <cocos/network/HttpResponse.h>
#include "BUserInfor.h"
#include "ShopItem.h"
#include "FacebookHandler.h"
#include <iostream>

USING_NS_CC;
using namespace cocos2d::network;
using namespace std;
enum TAG{
    TAG_FRIEND,TAG_WORLD
};




class ParseDelegate{
public:
	//Leaderboard
    virtual void responseForQuerryTopFriend(vector<BUserInfor*> friendList){

    };
    virtual void responseForQuerryTopWorld(vector<BUserInfor*> worldList){

    };
    virtual void responseAfterCheckFacebookIDExistOnParse(){

      };
    virtual void responseAfterCheckInstalledGamesOnParse(){
      };

    //Shop
    virtual void responseForQuerryShopItems(){
      };
    
    //All Scene
    virtual void responseAfterGetCoinsFromParse( int score ){
    };
    virtual void responseAfterFetchInternetStatus(bool isConnected){
    };
    
    
};


class ParseHandler: public FacebookDelegate
{
private:
    TAG tag;
public:
    ParseHandler();
    ~ParseHandler();
    CC_SYNTHESIZE(ParseDelegate*,_parseDelegate,ParseDelegate);
    CC_SYNTHESIZE(vector<BUserInfor*>,_worldList,WorldList);
    CC_SYNTHESIZE(vector<BUserInfor*>,_friendList,FriendList);
    static ParseHandler* getInstance();
    void fetchScoreFromServer();
    void putScoreToSever(int score);
    void submitScore(int score);
    int scoreToSubmit;


    //Leaderboard
    void checkFacebookIdExistOnParse();
    void checkFacebookIdExistOnParseCallBack(HttpClient* client,HttpResponse* response);

    void saveFacebookIdOnParse(BUserInfor* user);
    void callBacksaveFacebookIdOnParse(HttpClient* client,HttpResponse* response);

    void fetchTopFriend();
    void fetchTopWorld();
    void fetchBUserInforAt(char* querry);
    void callBackFetchBUserInforAt(HttpClient* client,HttpResponse* response);

    void checkInstalledGames();
    void checkInstalledGameCallback(HttpClient* client,HttpResponse* response);
    void submitInstalledGame(string gameName);
    bool isSubmitingInstalledGame = false;

    //Shop
    void getAvailableItemsOnServer();
    void getAvailableItemsOnServerCallback(HttpClient* client,HttpResponse* response);
    void getAnimationImagesFromServer(string itemName);
    void getAnimationImagesFromServerCallback(HttpClient* client,HttpResponse* response);

    //Facebook delegate
    virtual void responseWhenGetFriendsSuccessfully(string friendList);
    virtual void responseWhenGetMyInfoSuccessfully(BUserInfor* user);
    
    //Get coin from parse
    void fetchScoreFromParse();
    void isInternetConnect();
    CC_SYNTHESIZE(bool, isConnect, isNetAvailable);
    
};

#endif
