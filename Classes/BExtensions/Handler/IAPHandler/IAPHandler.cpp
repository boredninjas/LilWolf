

#include "IAPHandler.h"

IAPHandler::IAPHandler()
{

}

IAPHandler::~IAPHandler()
{
    
}
void IAPHandler::configIAP()
{
	sdkbox::IAP::setDebug(true);
	sdkbox::IAP::setListener(this);
	sdkbox::IAP::restore();
	sdkbox::IAP::refresh();
}

void IAPHandler::purchaseItem(string itemID)
{
	sdkbox::IAP::purchase(itemID);
}
IAPHandler* IAPHandler::getInstance()
{
    static IAPHandler* instance;
    if (instance==nullptr) {
        instance=new IAPHandler();
    }
    return instance;
}



//Callbacks
void IAPHandler::onRestoreComplete(bool ok, const std::string &msg) {
	CCLog("bambi IAP: onRestoreComplete");
}
void IAPHandler::onInitialized(bool ok) {
	CCLog("bambi IAP: onInitialized");
}
void IAPHandler::onSuccess(sdkbox::Product const& p) {
	CCLog("bambi IAP: onSuccess");
	if(_IAPDelegate != nullptr)
		_IAPDelegate->responseWhenGetItemSuccessfully(p.id);
}
void IAPHandler::onFailure(sdkbox::Product const& p, const std::string &msg) {
	CCLog("bambi IAP: onFailure");
}
void IAPHandler::onCanceled(sdkbox::Product const& p) {
	CCLog("bambi IAP: onCanceled");
}
void IAPHandler::onRestored(sdkbox::Product const& p) {
	CCLog("bambi IAP: onRestored");
	if(_IAPDelegate != nullptr)
			_IAPDelegate->responseWhenGetItemSuccessfully(p.id);

}
void IAPHandler::onProductRequestSuccess(
		std::vector<sdkbox::Product> const &products) {
	CCLog("bambi IAP: onProductRequestSuccess");
}
void IAPHandler::onProductRequestFailure(const std::string &msg) {
	CCLog("bambi IAP: onProductRequestFailure");
}
