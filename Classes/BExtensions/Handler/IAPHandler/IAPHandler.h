#ifndef __IAPHandler__
#define __IAPHandler__

#include "cocos2d.h"
#include "PluginIAP/PluginIAP.h"
USING_NS_CC;
using namespace std;

class IAPDelegate{
public:
    virtual void responseWhenGetItemSuccessfully(string itemID){
    };
};

class IAPHandler: public sdkbox::IAPListener {
public:
	IAPHandler();
	~IAPHandler();
	static IAPHandler* getInstance();
	void configIAP();
	void purchaseItem(string itemID);
    CC_SYNTHESIZE(IAPDelegate*,_IAPDelegate,IAPDelegate);

	//IAP listener
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg)
			override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(
			std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
	virtual void onRestoreComplete(bool ok, const std::string &msg) override;
};

#endif
